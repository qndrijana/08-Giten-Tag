const db = require('./db');

const commitSchema = new db.Schema({
    _id: db.Schema.Types.ObjectId,
    date: {
        type: db.Schema.Types.Date,
        required: true,
    },
    repo: {
        type: db.Schema.Types.ObjectId,
        ref : 'Repo'
    },
    repoName: {
        type: db.Schema.Types.String,
    },
    // techStats: [{
    //     type: db.Schema.Types.ObjectId,
    //     ref = 'techstatsSchema',
    // }],
    username: {
        type: db.Schema.Types.String
    },
    mail: {
        type: db.Schema.Types.String
    },
    author: {
        type: db.Schema.Types.ObjectId,
        ref: 'User'
    },
    numLinesAdded: {
        type: db.Schema.Types.Number,
        default: 0
    },
    numLinesDeleted: {
        type: db.Schema.Types.Number,
        default: 0
    }
});

const Commit = db.model('Commit', commitSchema);
module.exports = Commit;
