const db = require('./db');

const userSchema = new db.Schema({
    _id: db.Schema.Types.ObjectId,
    username: {
        type: db.Schema.Types.String,
        required: true,
        unique: true
    },
    password: {
        type: db.Schema.Types.String,
        required: true,
    },
    email: {
        type: db.Schema.Types.String,
        required: true,
        unique: true
    },
    location: {
        type: db.Schema.Types.String
    },
    dateOfBirth: {
        type: db.Schema.Types.Date
    },
    githubUrl: {
        type: db.Schema.Types.String,
    },
    githubAvatarUrl: {
        type: db.Schema.Types.String,
    },
    privateFields: {
        type: db.Schema.Types.Array
    },
    tokenInvalidBefore: {
        type: db.Schema.Types.Date,
        default: 0
    },
    githubToken: {
        type: db.Schema.Types.String
    },
    githubEmails: {
        type: db.Schema.Types.Array
    },
    isRepoUpdateInProgress: {
        type: db.Schema.Types.Boolean
    },
    repos: [{
        type: db.Schema.Types.ObjectId,
        ref: 'Repo'
     }]
});

const User = db.model('User', userSchema);
module.exports = User;
