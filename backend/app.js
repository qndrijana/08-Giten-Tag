const express = require('express');
const cors = require('cors')
const usersRouter = require('./routes/api/users');
const usersController = require('./controllers/users');
const githubRouter = require('./routes/api/github');
const repoRouter = require('./routes/api/repo')

const { urlencoded, json } = require('body-parser');
const jwt = require('express-jwt');

const UNPROTECTED_PATHS = ['/api/users/register', '/api/users/login', '/api/github/callback', '/api/github/login',
    '/api/repo/getCommits', '/api/repo/getBasicData', '/api/repo/getMostProductiveDay', '/api/repo/getRepoObjects',
    '/api/repo/getMostProductivePeriod', '/api/repo/getLangInfo', '/api/users'];

const app = express();

app.use(json());
app.use(cors()); //todo scope down CORS
app.use(urlencoded({ extended: false }));
app.use(jwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'], isRevoked: usersController.isTokenRevoked})
    .unless({path: UNPROTECTED_PATHS}));

app.use('/api/users', usersRouter);
app.use('/api/github', githubRouter);

app.use('/api/repo', repoRouter)

app.use(function (req, res, next) {
    const error = new Error('Not found!');
    error.status = 404;

    next(error);
});

app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
        message: error.message,
        status: statusCode,
        stack: process.env.DEBUG ? error.stack : undefined
    });
});

module.exports = app;
