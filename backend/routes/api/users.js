const express = require('express');
const controller = require('../../controllers/users');


const router = express.Router();

router.post('/register', controller.registerUser);
router.post('/login', controller.loginUser);
router.post('/refresh', controller.refreshToken);
router.post('/password/reset', controller.resetPassword);
router.get('/', controller.getUserInfo);
router.put('/', controller.editInfo);
router.delete('/', controller.deleteUser);

module.exports = router;
