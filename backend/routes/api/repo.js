const express = require('express');
const controller = require('../../controllers/repo');

const router = express.Router();

router.get('/getCommits', controller.getAllCommitsInfo);
router.get('/getBasicData', controller.getBasicUserData);
router.get('/getMostProductiveDay', controller.getUserMostProductiveDay);
router.get('/getRepoObjects', controller.getAllUserRepoObjects);
router.get('/getMostProductivePeriod', controller.getUserMostProductivePeriod);
router.get('/getLangInfo', controller.getLangInfo);

module.exports = router;