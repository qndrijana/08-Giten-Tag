const Repo = require('../models/repo');
const mongoose = require('mongoose');

const addRepo = async(name, mapLangToNumOfLines, mapLangToNumOfCommits, username, description, url, repoName) => {
    const repos = new Repo({
        _id: new mongoose.Types.ObjectId(),
        mapLangToNumOfCommits: mapLangToNumOfCommits,
        username: username,
        description: description,
        mapLangToNumOfLines : mapLangToNumOfLines,
        url: url,
        name: repoName
    });
    repos.markModified('mapLangToNumOfCommits');
    repos.markModified('mapLangToNumOfLines');
    await repos.save();
    return repos;
};

const deleteReposForUser = async(username) => {
    return Repo.deleteMany({username: username}).exec();
}

const userNumberOfRepos = async(username) => {
    return Repo.countDocuments({username: username}).exec();
};

const getAllUserRepos = async(username) => {
    return Repo.find({username: username}).select('name url description numOfLines mapLangToNumOfCommits mapLangToNumOfLines -_id').exec();
};

const getAllUsedLanguagesLines = async(username) => {
    return Repo.find({username: username}).select('mapLangToNumOfLines -_id').exec();
}

const getAllUsedLanguagesCommits = async(username) => {
    return Repo.find({username: username}).select('mapLangToNumOfCommits -_id').exec();
}

module.exports = {addRepo, userNumberOfRepos, getAllUserRepos, getAllUsedLanguagesLines, getAllUsedLanguagesCommits, deleteReposForUser};
