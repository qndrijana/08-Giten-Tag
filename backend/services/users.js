const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');
const argon2 = require('argon2');
const User = require('../models/user');

const ARGON2_OPTS = {
    type: argon2.argon2id
};
const JWT_OPTS = {
    algorithm: 'HS256',
    expiresIn: '6h'
};
const JWT_REFRESH_OPTS = {
    algorithm: 'HS256',
    expiresIn: '24h'
};

const JWT_SECRET = process.env.JWT_SECRET;

class UnauthenticatedUserException extends Error {
    constructor(message) {
        super(message);
        this.name = "UnauthenticatedUserException";
    }
}

const getUser = async (email) => {
    return await User.findOne({email: email}).exec();
}

const getUserByUsername = async (username) => {
    return await User.findOne({username: username}).exec();
}

const registerUser = async (username, email, password, location, dateOfBirth) => {
    const hashedPassword = await argon2.hash(password, ARGON2_OPTS);
    let birthdate = (dateOfBirth == null || dateOfBirth === '') ? undefined : new Date(dateOfBirth);
    const newUser = new User({
        _id: new mongoose.Types.ObjectId(),
        username: username,
        email: email,
        password: hashedPassword,
        location: location,
        dateOfBirth: birthdate
    });

    await newUser.save();
    return newUser;
};

const loginUser = async (email, password) => {
    const user = await User.findOne({email: email}).exec();
    if(!user) throw new UnauthenticatedUserException("User doesn't exist!");
    const passwordOkay = await argon2.verify(user.password, password, ARGON2_OPTS);
    if(passwordOkay) return user;
    else throw new UnauthenticatedUserException("User doesn't exist!");
}

const resetPassword = async (email, oldPassword, newPassword) => {
    const user = await loginUser(email, oldPassword);
    user.password = await argon2.hash(newPassword, ARGON2_OPTS);
    user.tokenInvalidBefore = Date.now();
    await user.save();
    return user;
}

const generateToken = (user) => {
    const accessToken = jwt.sign({
        sub: user.email,
        type: 'access'
    }, JWT_SECRET, JWT_OPTS);
    const refreshToken = jwt.sign({
        sub: user.email,
        type: 'refresh'
    }, JWT_SECRET, JWT_REFRESH_OPTS);
    return {
        accessToken: accessToken,
        refreshToken: refreshToken
    }
}

module.exports = {
    UnauthenticatedUserException,
    getUser,
    registerUser,
    loginUser,
    resetPassword,
    generateToken,
    getUserByUsername
}


