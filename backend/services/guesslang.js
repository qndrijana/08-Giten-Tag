const { ModelOperations } = require('@vscode/vscode-languagedetection');
const mongoose = require('mongoose');
const Repo = require('../models/repo');
const path = require('path');
const fs = require('fs');
const { isBinary } = require('istextorbinary');

function ThroughDirectory(Directory, result) {
    fs.readdirSync(Directory).forEach(File => {
        const Absolute = path.join(Directory, File);
        if (File === '.git') return;
        else if (fs.statSync(Absolute).isDirectory()) return ThroughDirectory(Absolute, result);
        else if (!isBinary(File)) result.push(Absolute);
    });
    return result;
}

function numOfLinesInFile(filename) {
    var data = fs.readFileSync(filename);
    var res = data.toString().split('\n').length;
    return res - 1;
}

const langOfFiles = async (repoName) => {
    var result1 = [];
    repoName = path.resolve(repoName);
    var result = ThroughDirectory(repoName, result1);
    var mapPathToLang = {};
    var mapNumOfLinesToLang = {};
    const modulOperations = new ModelOperations();

    mapLangIdToFullName = {
        'cpp': 'C++',
        'c': 'C',
        'py': 'Python',
        'js': 'JavaScript',
        'kt': 'Kotlin',
        'coffee': 'CoffeeScript',
        'ts': 'TypeScript',
        'jl': 'Julia',
        'rb': 'Ruby',
        'hs': 'Haskell',
        'rs': 'Rust',
        'pl': 'Perl',
        'go' : 'GO',
        'makefile' : 'MakeFile',
        'ipynb': 'Jupyter',
        'erl': 'Erlang',
        'sh': 'Bash',
        'html': 'HTML',
        'css': 'CSS',
        'xml': 'XML',
        'md': 'Markdown',
        'yaml': 'YAML',
        'ini': 'INI',
        'json': 'JSON',
        'csv': 'CSV',
        'bat': 'Batch',
        'pas': 'Paskal',
        'tex': 'LaTeX',
        'java' : 'JAVA',
        'groovy' : 'GROOVY',
        'dart' : 'DART',
        'erl' : 'Erlang'
    }
    for (let file of result) {
        try {
            const data = fs.readFileSync(file, 'ASCII');
            const result = await modulOperations.runModel(data);
            if (result[0] !== undefined) {
                var { languageId } = result[0];

                if (mapLangIdToFullName.hasOwnProperty(languageId)) {
                    languageId = mapLangIdToFullName[languageId];
                }

                mapPathToLang[file] = languageId;

                if (!mapNumOfLinesToLang[languageId])
                    mapNumOfLinesToLang[languageId] = numOfLinesInFile(file);
                else {
                    mapNumOfLinesToLang[languageId] += numOfLinesInFile(file);

                }
            }

        } catch (err) {
            console.error(err)
        }
    }
    returnValue = [];
    returnValue.push(mapNumOfLinesToLang)
    returnValue.push(mapPathToLang);
    return returnValue;
};

function returnMapPathToLang() {

}

module.exports = { langOfFiles };

